defmodule Library.BookController do
    use Library.Web, :controller

    alias Ecto.{Changeset}
    alias Library.{Repo,Book}
  
    def index(conn, _params) do
      books = Repo.all(from b in Book)

      conn = conn |> put_status(200)
      render conn, "loans.html", books: books
    end

    def extend(conn, %{"id" => book_id}) do
        book = Repo.get(Book, book_id)
        books = Repo.all(from b in Book)

        if book.times_extended == 4 do
            conn = conn
             |> put_status(400)
             |> put_flash(:error, "Extension rejected")

             render conn, "loans.html", books: books
        else
            if book.code == "A" do
                due_date = book.due_date
                date = Date.add(due_date, 7)
    
                post = Ecto.Changeset.change book, due_date: date
                Repo.update post
            end
    
            if book.code == "B" do
                due_date = book.due_date
                date = Date.add(due_date, 14)
    
                post = Ecto.Changeset.change book, due_date: date
                Repo.update post
            end
    
            if book.code == "C" do
                due_date = book.due_date
                date = Date.add(due_date, 21)
    
                post = Ecto.Changeset.change book, due_date: date
                Repo.update post
            end
    
            times = book.times_extended
            times = times + 1

            post = Ecto.Changeset.change book, times_extended: times
            Repo.update post

            conn = conn 
                |> put_status(200)
                |> put_flash(:info, "Extension accepted")
    
            render conn, "loans.html", books: books
        end
    end
  end
  