alias Library.{Repo, Book}

[%{title: "Programming Phoenix 13", code: "A", start_date: ~D[2018-01-19], due_date: ~D[2018-01-16], times_extended: 0},
%{title: "Secrets of the JavaScript ninja", code: "A", start_date: ~D[2017-12-19], due_date: ~D[2018-01-16], times_extended: 4}]
|> Enum.map(fn book -> Book.changeset(%Book{}, book) end)
|> Enum.each(fn changeset -> Repo.insert!(changeset) end)