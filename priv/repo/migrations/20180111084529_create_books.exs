defmodule Library.Repo.Migrations.CreateBooks do
  use Ecto.Migration

  def change do
    create table(:book) do
      add :title, :string
      add :code, :string
      add :start_date, :date
      add :due_date, :date
      add :times_extended, :int

      timestamps()
    end

    create unique_index(:book, [:title])
  end
end
