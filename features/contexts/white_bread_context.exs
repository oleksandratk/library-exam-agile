defmodule WhiteBreadContext do
    use WhiteBread.Context
    use Hound.Helpers
    alias Library.{Repo, Book}
  
    feature_starting_state fn  ->
      Application.ensure_all_started(:hound)
      %{}
    end
    scenario_starting_state fn _state ->
      Hound.start_session
      Ecto.Adapters.SQL.Sandbox.checkout(Library.Repo)
      Ecto.Adapters.SQL.Sandbox.mode(Library.Repo, {:shared, self()})
      %{}
    end
    scenario_finalize fn _status, _state ->
      Ecto.Adapters.SQL.Sandbox.checkin(Library.Repo)
      Hound.end_session
    end
  
    given_ ~r/^exsiting books$/,
    fn state, %{table_data: table} ->
      table
      |> Enum.map(fn book -> Book.changeset(%Book{}, book) end)
      |> Enum.each(fn changeset -> Repo.insert!(changeset) end)
      {:ok, state}
    end
  
    and_ ~r/^I go to the main page$/, fn state ->
      navigate_to "/loans/"
      {:ok, state}
    end
  
    when_ ~r/^I select "(?<id>[^"]+)" to extend a book$/, 
    fn state, %{id: id} ->  
       
      element = find_element(:id, id)
      click(element)
      
      {:ok, state}
    end
  
    then_ ~r/^I receive a "(?<notification>[^"]+)"$/,
    fn state, %{notification: notification} ->
      assert visible_in_page? Regex.compile!(notification)
  
      {:ok, state}
    end
  end
  