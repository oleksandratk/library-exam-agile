Feature: Extend a book
    As a library memeber
    I want to extend a book

    Scenario Outline: Extend
        Given exsiting books
        | Title                                 | Code | Start Date | Due Date   | Time Extended |
        | Programming Phoenix 13                | A    | 09/01/2018 | 16/01/2018 | 0             |
        | Secrets of the JavaScript ninja       | A    | 19/12/2017 | 16/01/2018 | 4             |

        And I go to the main page
        When I select "<id>" to extend a book
        Then I receive a "<notification>"
    
    Examples:
        | id | title                                 | notification          |
        | 1  | Programming Phoenix 13                | Extension accepted    |
        | 2  | Secrets of the JavaScript ninja       | Extension rejected    |