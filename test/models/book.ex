defmodule Library.Book do
    use Library.Web, :model

    schema "book" do
        field :title, :string
        field :code, :string
        field :start_date, :date
        field :due_date, :date
        field :times_extended, :integer

        timestamps()
    end

    def changeset(struct, params \\ %{}) do
        struct
        |> cast(params, [:title, :code, :start_date, :due_date, :times_extended])
        |> unique_constraint(:title, title: :book_title_index, message: "This book already exists.")
    end
end