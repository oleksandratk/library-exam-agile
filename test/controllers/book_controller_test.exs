defmodule Library.BookControllerTest do
    use Library.ConnCase
    alias Library.{Repo, Book}
  
    test "GET /loans", %{conn: conn} do
      conn = get conn, "/loans"
      assert html_response(conn, 200)
    end

    test "GET /loans with small amount of extended times", %{conn: conn} do
        [%{title: "Programming Phoenix 13", code: "A", start_date: ~D[2018-01-19], due_date: ~D[2018-01-16], times_extended: 0}]
        |> Enum.map(fn book -> Book.changeset(%Book{}, book) end)
        |> Enum.each(fn changeset -> Repo.insert!(changeset) end)

        get conn, "/loans/:id", %{id: "1"}
        assert html_response(conn, 200) =~ ~r/Extension accepted/
    end

    test "GET /loans with big amount of extended times", %{conn: conn} do
        [%{title: "Programming Phoenix 13", code: "A", start_date: ~D[2018-01-19], due_date: ~D[2018-01-16], times_extended: 4}]
        |> Enum.map(fn book -> Book.changeset(%Book{}, book) end)
        |> Enum.each(fn changeset -> Repo.insert!(changeset) end)

        get conn, "/loans/:id", %{id: "1"}
        assert html_response(conn, 400) =~ ~r/Extension rejected/
    end
  end
  